#!/bin/bash

# Init vars
WS=$(terraform workspace show)
IPK8S=$(terraform output -json control_plane_public_ip | jq -j)

echo -e "\n##################################################################"
echo -e "# Copy .kube/config from ControlPlane to local pc for kubectl"
echo -e "####################################################################\n"
ssh -o "StrictHostKeyChecking no" ilyamarin@$IPK8S "sudo cp /root/.kube/config /home/ilyamarin/config; sudo chown ilyamarin /home/ilyamarin/config"
scp ilyamarin@$IPK8S:/home/ilyamarin/config "./config.${WS}"
sed -i "s/lb-apiserver.kubernetes.local/${IPK8S}/g" "./config.${WS}"
export KUBECONFIG="./config.$WS"

echo -e "\n##################################################################"
echo -e "# Install kube-prometheus"
echo -e "####################################################################\n"
kubectl apply --server-side -f ../kube-prometheus/manifests/setup
until kubectl get servicemonitors --all-namespaces ; do date; sleep 1; echo ""; done
kubectl apply -f ../kube-prometheus/manifests/
kubectl apply -f ../kube-prometheus/grafana-svc.yaml -f ../kube-prometheus/grafana-NP.yaml

echo -e "\n##################################################################"
echo -e "# Create namespace for application"
echo -e "####################################################################\n"
kubectl create ns $WS

echo -e "\n##################################################################"
echo -e "# copy .kube/config to app repo"
echo -e "####################################################################\n"
curl --request PUT --header "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" \
    "https://gitlab.com/api/v4/projects/oliver.chaffinch%2Ftest_app/variables/CI_KUBE_CONFIG" \
    --form "value=$(cat $KUBECONFIG)" -o /dev/null
curl --request PUT --header "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" \
    "https://gitlab.com/api/v4/projects/oliver.chaffinch%2Ftest_app/variables/CI_WORKSPACE" \
    --form "value=${WS}" -o /dev/null

